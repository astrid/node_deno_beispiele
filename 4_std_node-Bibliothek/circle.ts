import { createRequire } from "https://deno.land/std@0.152.0/node/module.ts";

// import.meta.url ist der Ort des Moduls "this" (wie `__filename` in
// Node), und dient dann als Wurzel für Ihr "Paket", wo die
// `package.json` erwartet wird, und wo `node_modules` für die Auflösung von Paketen verwendet wird
// für die Auflösung von Paketen.
const require = createRequire(import.meta.url);

// Lädt das eingebaute Modul Deno "replacement":
const path = require("path");

// Lädt ein CommonJS-Modul (ohne Angabe der Erweiterung):
// Man kann die CommonJS-Module auch ohne Angabe der Dateierweiterung laden.
//const cjsModule = require("./my_mod");

const circle = require('./circle');
console.log(`The area of a circle of radius 5 is ${circle.area(5)}`);

// Verwendet die Node-Auflösung in `node_modules`, um das Paket/Modul zu laden. Das
// Paket müsste lokal über ein Paketmanagement-Tool installiert werden
// wie npm:

const maplibre = require("maplibre-gl");
console.log(maplibre);
