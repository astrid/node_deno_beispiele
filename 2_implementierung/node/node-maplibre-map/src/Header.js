import React from 'react';
import './Header.css';

export default function Header(){

  return (
    <div className="heading">
      <h1>MapLibre Map</h1>
      <p>mit Demotiles und einem Marker am höchsten Punkt der deutschen Eifel - der Hohen Acht.</p>
    </div>
  );
}
