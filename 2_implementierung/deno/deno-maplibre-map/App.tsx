import { React } from './deps.ts';
import Map from './Map.tsx';
import Header from './Header.tsx';

function App() {
  return (
    <>
      <div className="App">
        <Header />
        <Map />
      </div>
    </>
  );
}

export default App;
