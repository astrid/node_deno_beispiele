import { React } from './deps.ts';

function Header() {
  return (
    <>
      <div className="heading">
        <h1>MapLibre Map</h1>
        <p>mit Demotiles und einem Marker am höchsten Punkt der deutschen Eifel - der Hohen Acht.</p>
      </div>
    </>
  );
}

export default Header;
