import React from 'react';
import Map from './Map.js';
import Header from './Header.js';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header/>
      <Map/>
    </div>
  );
}

export default App;
