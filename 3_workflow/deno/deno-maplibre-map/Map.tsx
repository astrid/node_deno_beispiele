import { React } from './deps.ts';
//import maplibregl from "https://unpkg.com/maplibre-gl/dist/maplibre-gl.js";
//import maplibregl from "https://cdn.skypack.dev/maplibre-gl";
import maplibregl from "https://esm.sh/v91/maplibre-gl@2.3.0/es2022/maplibre-gl.js";

function Map() {
  const mapContainer = React.useRef(null);
  const map = React.useRef(null);
  const [lng] = React.useState(7.011028);
  const [lat] = React.useState(50.386139);
  const [zoom] = React.useState(5);


  React.useEffect(() => {
    if (map.current) return;
    map.current = new maplibregl.Map({
      container: mapContainer.current,
      style: `https://demotiles.maplibre.org/style.json`,
      center: [lng, lat],
      zoom: zoom
    });

    map.current.addControl(new maplibregl.NavigationControl(), 'top-right');

    new maplibregl.Marker({ color: "#FF0000" })
      .setLngLat([lng, lat])
      .addTo(map.current);

  }, []);


  return (
    <>
      <div className="map-wrap">
        <div ref={mapContainer} className="map"></div>
      </div>
    </>
  );
}

export default Map;
